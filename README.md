# Ogamebot

This is a fully ogame lobby compatible bot.

The bot features can be found in the [project's wiki](https://gitlab.com/Daklon/ogame-bot/wikis/home)

It's open source and is under the GPLv2. The contribution guide will be added in the future, any (well done) contribution is accepted.

## Downloads
* [Version 1.3.2](https://mega.nz/#!iC5SCI5I!F2JfvwP5jUcAPubnzAqASikjguEBZFObGV6YVInEJY8)
* [Version 1.3.1](https://mega.nz/#!qD4ymYqS!VMeHt1F6iU5KQI9Sa0P0gespWhYtgvoPYGgiaVKv_vU)
* [Version 1.3](https://mega.nz/#!PCwkSKSD!qSHBd_DC6ActGumrH-IxyLVW-H9KL_EVZ8rnVHl9TdM)
* [Version 1.2](https://mega.nz/#!6LgnUD5I!bRZhgzcUghYn8MoVDXKnrbbwkuWLRC_YVLAynfmKu7U)

## Usage 
The bot's documentation is in the [project's wiki](https://gitlab.com/Daklon/ogame-bot/wikis/home)

## Reports bugs and help
If you found any bug or has any suggestion, please, [open an issue](https://gitlab.com/Daklon/ogame-bot/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=). If you need, you can contact with me via mail at daklon AT disroot dot org.
