from selenium.common.exceptions import ElementClickInterceptedException

def scroll_click(driver, element):
    try:
        element.click()
    except ElementClickInterceptedException:
        x = element.location['x']
        y = element.location['y']
        scroll_by_coord = 'window.scrollTo(%s,%s);' % (
                x,
                y
                )
        driver.execute_script(scroll_by_coord)
        element.click()
    
    return element
